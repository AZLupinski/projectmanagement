package pl.sda.projectmanagement.projectManagementApp.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "projects")
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "project_id")
    private Long id;

    @Column(name = "project_name", nullable = false)
    private String title;

    @Column(name = "project_description")
    private String description;

    @OneToOne
    private User admin;

    @ManyToMany
    @JoinTable(name ="project_users",
    joinColumns = {@JoinColumn(name = "project_id", referencedColumnName = "project_id")},
    inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "user_id")})
    private List<User> users;

    @OneToMany(mappedBy = "project")
    private List<Task> tasks;

    @OneToMany(mappedBy = "project")
    private List<Sprint> sprints;

    @OneToMany
    @JoinColumn(name = "project_id", referencedColumnName="project_id")
    private List<Status> statuses;


    public Project() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getAdmin() {
        return admin;
    }

    public void setAdmin(User admin) {
        this.admin = admin;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Sprint> getSprints() {
        return sprints;
    }

    public void setSprints(List<Sprint> sprints) {
        this.sprints = sprints;
    }

    public List<Status> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<Status> statuses) {
        this.statuses = statuses;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Project)) return false;

        Project project = (Project) o;

        if (!getId().equals(project.getId())) return false;
        if (!getTitle().equals(project.getTitle())) return false;
        if (getDescription() != null ? !getDescription().equals(project.getDescription()) : project.getDescription() != null)
            return false;
        if (!getAdmin().equals(project.getAdmin())) return false;
        if (getUsers() != null ? !getUsers().equals(project.getUsers()) : project.getUsers() != null) return false;
        if (getTasks() != null ? !getTasks().equals(project.getTasks()) : project.getTasks() != null) return false;
        if (getSprints() != null ? !getSprints().equals(project.getSprints()) : project.getSprints() != null)
            return false;
        return getStatuses() != null ? getStatuses().equals(project.getStatuses()) : project.getStatuses() == null;
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getTitle().hashCode();
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + getAdmin().hashCode();
        result = 31 * result + (getUsers() != null ? getUsers().hashCode() : 0);
        result = 31 * result + (getTasks() != null ? getTasks().hashCode() : 0);
        result = 31 * result + (getSprints() != null ? getSprints().hashCode() : 0);
        result = 31 * result + (getStatuses() != null ? getStatuses().hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", admin=" + admin +
                ", users=" + users +
                ", tasks=" + tasks +
                ", sprints=" + sprints +
                ", statuses=" + statuses +
                '}';
    }
}
