package pl.sda.projectmanagement.projectManagementApp.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "sprints")
public class Sprint implements Serializable {
    private static final long serialVersionUID = 3L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sprint_id")
    private Long id;

    @Column(name = "sprint_title", nullable = false)
    private String title;

    @Column(name = "sprint_start_date", nullable = false)
    private LocalDateTime startTime;

    @Column(name = "sprint_end_time", nullable = false)
    private LocalDateTime endTime;

    @Column(name = "sprint_story_point", nullable = false)
    private Long storyPoints;

    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    public Sprint(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Long getStoryPoints() {
        return storyPoints;
    }

    public void setStoryPoints(Long storyPoints) {
        this.storyPoints = storyPoints;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sprint)) return false;

        Sprint sprint = (Sprint) o;

        if (!getId().equals(sprint.getId())) return false;
        if (!getTitle().equals(sprint.getTitle())) return false;
        if (!getStartTime().equals(sprint.getStartTime())) return false;
        if (!getEndTime().equals(sprint.getEndTime())) return false;
        if (!getStoryPoints().equals(sprint.getStoryPoints())) return false;
        return getProject().equals(sprint.getProject());
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getTitle().hashCode();
        result = 31 * result + getStartTime().hashCode();
        result = 31 * result + getEndTime().hashCode();
        result = 31 * result + getStoryPoints().hashCode();
        result = 31 * result + getProject().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Sprint{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", storyPoints=" + storyPoints +
                ", project=" + project.getTitle() +
                '}';
    }
}
