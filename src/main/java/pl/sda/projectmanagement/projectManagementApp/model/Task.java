package pl.sda.projectmanagement.projectManagementApp.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tasks")
public class Task implements Serializable {
    private static final long serialVersionUID = 5L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id")
    private Long id;

    @Column(name = "task_title", nullable = false)
    private String title;

    @Column(name = "task_description", nullable = false)
    private String description;

    @Column(name = "task_weight", nullable = false)
    private int weight;

    @Column(name = "task_story_points", nullable = false)
    private String storyPoints;

    @ManyToOne
    @JoinColumn(name = "status_id", nullable = false)
    private Status status;

    @ManyToMany
    @JoinTable(name = "task_user",
            joinColumns = {@JoinColumn(name = "task_id", referencedColumnName = "task_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "user_id")})
    private List<User> attachedUsers;

    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;


    public Task(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getStoryPoints() {
        return storyPoints;
    }

    public void setStoryPoints(String storyPoints) {
        this.storyPoints = storyPoints;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<User> getAttachedUsers() {
        return attachedUsers;
    }

    public void setAttachedUsers(List<User> attachedUsers) {
        this.attachedUsers = attachedUsers;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;

        Task task = (Task) o;

        if (getWeight() != task.getWeight()) return false;
        if (!getId().equals(task.getId())) return false;
        if (!getTitle().equals(task.getTitle())) return false;
        if (!getDescription().equals(task.getDescription())) return false;
        if (!getStoryPoints().equals(task.getStoryPoints())) return false;
        if (!getStatus().equals(task.getStatus())) return false;
        if (getAttachedUsers() != null ? !getAttachedUsers().equals(task.getAttachedUsers()) : task.getAttachedUsers() != null)
            return false;
        return getProject().equals(task.getProject());
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getTitle().hashCode();
        result = 31 * result + getDescription().hashCode();
        result = 31 * result + getWeight();
        result = 31 * result + getStoryPoints().hashCode();
        result = 31 * result + getStatus().hashCode();
        result = 31 * result + (getAttachedUsers() != null ? getAttachedUsers().hashCode() : 0);
        result = 31 * result + getProject().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", weight=" + weight +
                ", storyPoints='" + storyPoints + '\'' +
                ", status=" + status.getName() +
                ", attachedUsers=" + attachedUsers +
                ", project=" + project.getTitle() +
                '}';
    }
}
